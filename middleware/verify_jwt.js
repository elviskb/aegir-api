const jwt = require('jsonwebtoken')
const createError = require('http-errors')

const verifyToken = (req, res, next) => {
  const authHeader = req.headers.token

  if (authHeader) {
    const token = authHeader.split(' ')[1]
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
      if (err) throw createError(403, 'Invalid token!')
      req.user = user
      next()
    })
  } else {
    return createError(401, 'You are not authenticated')
  }
}

const verifyTokenAndAuthorization = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.id === req.params.id || req.user.isAdmin) {
      next()
    } else {
      createError.Forbidden('You need to be an administrator to do this. Contact your systems administrator')
    }
  })
}

const verifyTokenAndAdmin = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.isAdmin) {
      next()
    } else {
      createError.Forbidden('You need to be an administrator to do this. Contact your systems administrator')
    }
  })
}

module.exports = { verifyToken, verifyTokenAndAuthorization }