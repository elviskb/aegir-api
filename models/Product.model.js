const { Schema, model } = require('mongoose')

const productSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  imgUrl: {
    type: String,
    required: true,
    trim: true
  },
  categories: {
    type: Array
  },
  price: {
    type: Number,
    required: true,
    trim: true
  },
  inStock: {
    type: Boolean,
    default: true
  }
}, { timestamps: true })

module.exports = model('Product', productSchema)