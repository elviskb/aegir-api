const { Schema, model } = require('mongoose')

const cartSchema = new Schema({
  productId: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  }
  // userId: {
  //   type: String,
  //   required: true,
  //   trim: true
  // },
  // products: [
  //   {
  //     productId: {
  //       type: String
  //     },
  //     quantity: {
  //       type: Number,
  //       default: 1
  //     }
  //   }
  // ]
}, { timestamps: true })

module.exports = model('Cart', cartSchema)