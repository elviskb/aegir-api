const express = require("express");
const morgan = require("morgan");
const createError = require("http-errors");
const cors = require('cors')
require("dotenv").config();
require('./utilities/mongodb.init')

const app = express();

const PORT = process.env.PORT || 3000;

const userRoutes = require('./routes/user.routes')
const authRoutes = require('./routes/auth.routes')
const productRoutes = require('./routes/product.routes')
const orderRoutes = require('./routes/order.routes')
const cartRoutes = require('./routes/cart.routes')
const mailRoutes = require('./routes/mail.routes')
// const mpesaRoutes = require('./routes/mpesa.routes')

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(cors())

app.use('/api/v1/users', userRoutes)
app.use('/api/v1/auth', authRoutes)
app.use('/api/v1/products', productRoutes)
app.use('/api/v1/carts', cartRoutes)
app.use('/api/v1/orders', orderRoutes)
app.use('/api/v1/mail', mailRoutes)
// app.use('/api/v1/mpesa', mpesaRoutes)

app.use(async (req, res, next) => {
  next(createError.NotFound());
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});