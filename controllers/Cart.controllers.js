const createError = require('http-errors')
const Cart = require('../models/Cart.model')

module.exports.cartControllers = {
  // GET products
  getCarts: async (req, res, next) => {
    try {
      const carts = await Cart.find()

      if (!carts) throw createError.NotFound('Not carts created yet')

      res.status(200).json(carts)
    } catch (error) {
      next(error)
    }
  },

  // Get a single cart
  getCart: async (req, res, next) => {
    try {
      const cart = await Cart.findOne({ userId: req.params.userId })

      if (!cart) throw createError.NotFound('Cart has no products')

      res.status(200).json(cart)
    } catch (error) {
      next(error)
    }
  },

  // Create a Cart
  createCart: async (req, res, next) => {
    // console.log(req.body)
    const newCart = new Cart({ ...req.body })
    try {
      const savedCart = await newCart.save()

      if (!savedCart) throw createError(500, 'Product save failed. Try again')

      res.status(201).json(savedCart)
    } catch (error) {
      next(error)
    }
  },

  // Update the cart
  updateCart: async (req, res, next) => {
    try {
      const updatedCart = await Cart.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, { new: true })

      if (!updatedCart) throw createError(500, 'Update failed. Try again!')

      res.status(200).json(updatedCart)
    } catch (error) {
      next(error)
    }
  },

  // Delete
  deleteCart: async (req, res, next) => {
    try {
      const deletedCart = await Cart.findByIdAndDelete(req.params.id)

      if (!deletedCart) throw createError(500, 'Delete failed. Try again!')

      res.status(200).json('Cart deleted!')
    } catch (error) {
      next(error)
    }
  }
}