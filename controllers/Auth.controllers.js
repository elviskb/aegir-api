const CryptoJS = require('crypto-js')
const User = require('../models/User.model')
const createError = require('http-errors')
const jwt = require('jsonwebtoken')

module.exports.authControllers = {
  // register a new user
  registerUser: async (req, res, next) => {
    try {
      // Encrypt password with crypto-JS
      const password = CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SECRET)
      // Add user input to the database
      const newUser = await User(
        {
          ...req.body,
          password
        }
      )

      const savedUser = await newUser.save()

      if (savedUser) {
        console.log(`User saved: ${savedUser}`)
        res.status(201).json(savedUser)
      }

    } catch (error) {
      next(error)
    }
  },

  login: async (req, res, next) => {
    try {
      // Check if user exists in the db
      const user = await User.findOne({ username: req.body.username })

      if (!user) throw createError.Forbidden('Wrong user credentials. Try again.')

      // Decrypt password
      const hashedPassword = CryptoJS.AES.decrypt(user.password, process.env.PASS_SECRET)

      const originalPassword = hashedPassword.toString(CryptoJS.enc.Utf8)

      if (originalPassword !== req.body.password) throw createError.Forbidden('Wrong user credentials. Try again.')

      const accessToken = jwt.sign({
        id: user._id,
        isAdmin: user.isAdmin
      }, process.env.JWT_SECRET, { expiresIn: '3d' })

      const { password, ...others } = user._doc
      // IF successful
      res.status(200).json({ ...others, message: 'Login successful', accessToken })

    } catch (error) {
      next(error)
    }
  },

  // GET all users
  getAllUsers: async (req, res, next) => {
    res.send('All users will be listed here soon')
  }
}