const createError = require('http-errors')
const User = require('../models/User.model')
module.exports.userControllers = {
  // GET all users
  getUsers: async (req, res, next) => {
    // send a query with new to get five latest items on the db
    const query = req.query.new

    try {
      const users = query ? await User.find().sort({ _id: -1 }).limit(5) : await User.find()

      if (users.length < 1) throw createError.NotFound('So empty. Create new users.')

      res.status(200).json(users)
    } catch (error) {
      next(error)
    }
  },

  // GET a single user
  getUser: async (req, res, next) => {
    try {
      const user = await User.findById(req.params.id)

      if (!user) throw createError.NotFound('The user does not exist.')

      res.status(200).json(user)
    } catch (error) {
      next(error)
    }
  },

  getStats: async (req, res, next) => {
    const date = new Date()
    const lastYear = new Date(date.setFullYear(date.getFullYear() - 1))

    try {
      const data = await User.aggregate([
        { $match: { createdAt: { $gte: lastYear } } },
        {
          $project: {
            month: { $month: "$createdAt" }
          }
        },
        {
          $group: {
            _id: "$month",
            total: { $sum: 1 }
          }
        }
      ])

      res.status(200).json(data)

    } catch (error) {
      next(error)
    }
  },

  // update user
  updateUser: async (req, res, next) => {
    try {

      if (req.user.password) {
        // Encrypt password with crypto-JS
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SECRET).toString()
      }

      try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })

        res.status(200).json(updatedUser)
      } catch (error) {
        next(error)
      }
    } catch (error) {
      next(error)
    }
  },

  deleteUser: async (req, res, next) => {
    try {
      const deletedUser = await User.findByIdAndDelete(req.params.id)

      if (!deletedUser) throw createError.InternalServerError('Deletion failed. Try again.')

      res.status(200).json({ message: 'User deleted.' })
    } catch (error) {
      next(error)
    }
  }
}