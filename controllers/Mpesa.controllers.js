const createError = require('http-errors')
let axios = require('axios')

module.exports.mpesaControllers = {
  getAccessToken: async (req, res, next) => {

    //form a buffer of the consumer key and secret
    let buffer = new Buffer.from(process.env.CONSUMER_KEY + ":" + process.env.CONSUMER_SECRET);

    let auth = `Basic ${buffer.toString('base64')}`;

    const url = process.env.OAUTH_TOKEN_URL

    try {
      const { data } = await axios.get(
        url,
        { "headers": { 'Authorization': auth } }
      )

      req.token = data['access_token'];

      return next();

    } catch (error) {
      next(error)
    }
  },

  lipaNaMpesaOnline: async (req, res, next) => {
    let token = req.token;
    let auth = `Bearer ${token}`;


    //getting the timestamp
    let timestamp = require('../middleware/timestamp.middleware').timestamp;

    let url = process.env.LIPA_NA_MPESA_URL;
    let bs_short_code = process.env.LIPA_NA_MPESA_SHORTCODE;
    let passkey = process.env.LIPA_NA_MPESA_PASSKEY;

    let password = new Buffer.from(`${bs_short_code}${passkey}${timestamp}`).toString('base64');
    let transcation_type = "CustomerPayBillOnline";
    let amount = "1"; //you can enter any amount
    let partyA = "party-sending-funds"; //should follow the format:2547xxxxxxxx
    let partyB = process.env.lipa_na_mpesa_shortcode;
    let phoneNumber = "party-sending-funds"; //should follow the format:2547xxxxxxxx
    let callBackUrl = "your-ngrok-url/mpesa/lipa-na-mpesa-callback";
    let accountReference = "lipa-na-mpesa-tutorial";
    let transaction_desc = "Testing lipa na mpesa functionality";

    try {
      let { data } = await axios.post(url, {
        "BusinessShortCode": bs_short_code,
        "Password": password,
        "Timestamp": timestamp,
        "TransactionType": transcation_type,
        "Amount": amount,
        "PartyA": partyA,
        "PartyB": partyB,
        "PhoneNumber": phoneNumber,
        "CallBackURL": callBackUrl,
        "AccountReference": accountReference,
        "TransactionDesc": transaction_desc
      }, {
        "headers": {
          "Authorization": auth
        }
      }).catch(console.log);

      return res.send({
        success: true,
        message: data
      });

    } catch (error) {
      next(error)
    }
  },

  lipaNaMpesaOnlineCallback: async (req, res, next) => {
    //Get the transaction description
    let message = req.body.Body.stkCallback['ResultDesc'];

    return res.send({
      success: true,
      message
    });
  }
}

