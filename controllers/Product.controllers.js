const createError = require('http-errors')
const Product = require('../models/Product.model')
const cloudinary = require('../utilities/cloudinary.helper')

module.exports.productControllers = {
  // GET products
  getProducts: async (req, res, next) => {
    const qNew = req.query.new
    const qCategory = req.query.category

    try {
      let products

      if (qNew) {
        products = await Product.find().sort({ createdAt: -1 }).limit(5)
      } else if (qCategory) {
        products = await Product.find({
          categories: {
            $in: [qCategory],
          }
        })
      } else {
        products = await Product.find()
      }

      if (products.length < 1) throw createError.NotFound('No products created yet, you might want to create some.')

      res.status(200).json(products)
    } catch (error) {
      next(error)
    }
  },
  // Get product
  getProduct: async (req, res, next) => {
    try {
      const product = await Product.findById({ _id: req.params.id })

      if (!product) throw createError.NotFound('Product not found')

      res.status(200).json(product)
    } catch (error) {
      next(error)
    }
  },
  // Create a product
  createProduct: async (req, res, next) => {
    try {
      const user_details = req.body
      const user_id = req.params.id

      // console.log('REQ>BODY:', req.body)
      // console.log('REQ.FILE:', req.file)
      let savedURI = null

      if (req.file) {
        const file = req.file
        const { path } = file

        const imageURI = await cloudinary.uploader.upload(path, {
          folder: "Aegir Solutions"
        })

        savedURI = imageURI.secure_url
        // console.log(`Image URIs: ${savedURI}`)
      }
      const newProduct = new Product({ ...req.body, imgUrl: savedURI })
      const savedProduct = await newProduct.save()

      if (!savedProduct) throw createError(500, 'Product save failed. Try again')

      res.status(201).json(savedProduct)
    } catch (error) {
      next(error)
    }
  },
  // Update a product
  updateProduct: async (req, res, next) => {
    try {
      const updatedProduct = await Product.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, { new: true })

      if (!updatedProduct) throw createError(500, 'Update failed. Try again!')

      res.status(200).json(updatedProduct)
    } catch (error) {
      next(error)
    }
  },
  // Delete
  deleteProduct: async (req, res, next) => {
    try {
      const deletedProduct = await Product.findByIdAndDelete(req.params.id)

      if (!deletedProduct) throw createError(500, 'Delete failed. Try again!')

      res.status(200).json('Product deleted!')
    } catch (error) {
      next(error)
    }
  }
}