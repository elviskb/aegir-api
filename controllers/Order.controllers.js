const createError = require('http-errors')
const Order = require('../models/Order.model')

module.exports.orderControllers = {
  // GET products
  getOrders: async (req, res, next) => {
    const qNew = req.query.new
    const qCategory = req.query.category

    try {
      const carts = await Cart.find()

      res.status(200).json(carts)
    } catch (error) {
      next(error)
    }
  },

  // Get user orders
  getOrder: async (req, res, next) => {
    try {
      const orders = await order.find({ userId: req.params.userId })

      if (!orders) throw createError.NotFound('Product not found')

      res.status(200).json(orders)
    } catch (error) {
      next(error)
    }
  },

  // GET MONTHLY INCOME
  getIncome: async (req, res, next) => {
    const date = new Date()
    const lastMonth = new Date(date.setMonth(date.getMonth() - 1))
    const previousMonth = new Date(new Date().setMonth(lastMonth.getMonth() - 1))
    try {
      const income = await Order.aggregate([
        { $match: { createdAt: { $gte: previousMonth } } },
        {
          $project:
          {
            month: { $month: "$createdAt" },
            sales: "$amount",
          },
        },
        {
          $group: {
            _id: "$month",
            total: { $sum: "$sales" },
          }
        },
      ]);

      res.send(income)
    } catch (error) {
      next(error)
    }
  },


  // Create a product
  createOrder: async (req, res, next) => {
    const newOrder = new Order({ ...req.body })
    try {
      const savedOrder = await newOrder.save()

      if (!savedOrder) throw createError(500, 'Product save failed. Try again')

      res.status(201).json(savedOrder)
    } catch (error) {
      next(error)
    }
  },
  // Update a product
  updateOrder: async (req, res, next) => {
    try {
      const updatedOrder = await Order.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, { new: true })

      if (!updatedOrder) throw createError(500, 'Update failed. Try again!')

      res.status(200).json(updatedOrder)
    } catch (error) {
      next(error)
    }
  },
  // Delete
  deleteOrder: async (req, res, next) => {
    try {
      const deletedOrder = await Order.findByIdAndDelete(req.params.id)

      if (!deletedOrder) throw createError(500, 'Delete failed. Try again!')

      res.status(200).json('Order deleted!')
    } catch (error) {
      next(error)
    }
  }
}