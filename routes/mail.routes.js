const router = require('express').Router()
const nodemailer = require('nodemailer')



router.post('/send', async (req, res, next) => {
  // console.log(req.body)

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'emailtest.avn@gmail.com',
      pass: 'xjzggdmydwrouszx'
    },
    tls: {
      rejectUnauthorized: false,
      ignoreTLS: true,
    }
  })

  const quoteRequestOutput = `
    <p>You have a new <b>Quote Request </b> from the Aegir Consult website shop</p>
    <h3>Contact Details</h3>
    <ul>
      <li>Name: ${req.body.name}</li>
      <li>Email address: ${req.body.from}</li>
      <li>Phone Number: ${req.body.tel}</li>
      <li>Service Requested: ${req.body.service}</li>
    </ul>
    <h3>Message</h3>
    <p>${req.body.text}</p>
  `

  const quoteRequestForm = `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
          @media only screen and (max-width: 620px) {
            h1 {
              font-size: 20px;
              padding: 5px;
            }
          }
        </style>
      </head>
      <body style="font-family: Arial, Helvetica, sans-serif ">
          <div
            style="max-width: 620px; background: rgb(247, 244, 244);  border: 5px solid #0C3C60; margin: 0 auto; color: #0C3C60">
            <p style="padding: 10px; text-align: center; font-weight: bold;">You have a new <b>QUOTE REQUEST </b> from the Aegir Consult website shop</p>
            <h3 style="background: #0C3C60; padding: 10px; text-align: center; color: #ffffff;">Contact Details</h3>
            <ul>
              <li>Name: ${req.body.name}</li>
              <li>Email address: ${req.body.from}</li>
              <li>Phone Number: ${req.body.tel}</li>
              <li>Service Requested: ${req.body.service}</li>
            </ul>
            <h3 style="background: #0C3C60; padding: 10px; text-align: center; color: #ffffff;">Message</h3>
            <div style="padding: 10px 30px 10px 30px;">
              <p>${req.body.text}</p>
            </div>
          </div>
        </body>
      </html>
    `



  const contactFormMessage = `
    <p>You have a new <b>Contact Message </b> from the Aegir Consult website contact form</p>
    <h3>Contact Details</h3>
    <ul>
      <li>Name: ${req.body.name}</li>
      <li>Email address: ${req.body.from}</li>
    </ul>
    <h3>Message</h3>
    <p>${req.body.text}</p>
  `

  const contactFrmMessage = `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
          @media only screen and (max-width: 620px) {
            h1 {
              font-size: 20px;
              padding: 5px;
            }
          }
        </style>
      </head>
      <body style="font-family: Arial, Helvetica, sans-serif ">
          <div
            style="max-width: 620px; background: rgb(247, 244, 244);  border: 5px solid #0C3C60; margin: 0 auto; color: #0C3C60">
            <p style="padding: 10px; text-align: center; font-weight: bold;">You have a new <b>CONTACT MESSAGE </b> from the
              Aegir Consult website Contact Form</p>
            <h3 style="background: #0C3C60; padding: 10px; text-align: center; color: #ffffff;">Contact Details</h3>
            <ul>
              <li>Name: ${req.body.name}</li>
              <li>Email address: ${req.body.from}</li>
            </ul>
            <h3 style="background: #0C3C60; padding: 10px; text-align: center; color: #ffffff;">Message</h3>
            <div style="padding: 10px 30px 10px 30px;">
              <p>${req.body.text}</p>
            </div>
          </div>
        </body>
      </html>
    `


  const mailOptions = {
    // from: process.env.USER_EMAIL,
    from: req.body.from,
    to: process.env.TO_EMAIL,
    subject: req.body.quote ? 'Quote Request ' + req.body.subject : 'From Aegir Website Contact Form',
    // name: req.body.name,
    // tel: req.body.tel,
    // service: req.body.service,
    // text: req.body.text
    html: req.body.quote ? quoteRequestForm : contactFrmMessage
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.log('An ERROR OCCURRED!:' + err)
      return
    }

    console.log('Sent: ' + info.response)
    res.status(200).json({
      message: 'Message delivered. \nThank you for reaching out to Aegir Consult. \nWe will be getting back to you shortly.'
    })
  })


  // const transporter = nodemailer.createTransport({
  //   //all of the configuration for making a site send an email.

  //   host: 'smtp.mailtrap.io',
  //   port: 465,
  //   secure: true,
  //   auth: {
  //     user: 'ee0f12330cceb2',
  //     pass: 'deecb5b34e1214'
  //   },
  //   tls: {
  //     rejectUnauthorized: false,
  //     ignoreTLS: true,
  //     // secureProtocol: 'TLSv1_method'
  //   }
  // })

  // const mailData = {
  //   from: req.body.from,
  //   to: req.body.to,
  //   subject: 'Quote Request' + req.body.subject || 'From Aegir Website Contact Form',
  //   name: req.body.name,
  //   tel: req.body.tel,
  //   service: req.body.service,
  //   text: req.body.text
  // };

  // transporter.sendMail(mailData, (error, info) => {
  //   if (error) {
  //     console.log(error)
  //   }
  //   res.status(200).json({
  //     message: 'Mail sent',
  //     // messageId: info.messageId
  //   })
  // }
  // )
})


module.exports = router