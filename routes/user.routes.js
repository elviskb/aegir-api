const router = require('express').Router()

const { userControllers } = require('../controllers/User.controllers')
const { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin } = require('../middleware/verify_jwt')


router.put('/:id', verifyToken, userControllers.updateUser)

router.delete('/:id', userControllers.deleteUser)

router.get('/find/:id', userControllers.getUser)

router.get('/', userControllers.getUsers)

// GET user stats ADMIN/PRIVATE route
router.get('/stats', userControllers.getStats)

module.exports = router