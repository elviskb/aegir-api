const { orderControllers } = require('../controllers/Order.controllers')

const router = require('express').Router()

router.get('/', orderControllers.getOrders)

router.get('/find/:id', orderControllers.getOrder)

router.post('/', orderControllers.createOrder)

router.put('/:id', orderControllers.updateOrder)

router.delete('/:id', orderControllers.deleteOrder)

router.get('/income', orderControllers.getIncome)

module.exports = router