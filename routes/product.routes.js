const { productControllers } = require('../controllers/Product.controllers')
const upload = require('../utilities/multer.helper')

const router = require('express').Router()

router.get('/', productControllers.getProducts)

router.get('/find/:id', productControllers.getProduct)

router.post('/', upload.single('image'), productControllers.createProduct)

router.put('/:id', productControllers.updateProduct)

router.delete('/:id', productControllers.deleteProduct)

// router.get('/', productControllers.getProducts)


module.exports = router