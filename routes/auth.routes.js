const { authControllers } = require('../controllers/Auth.controllers')

const router = require('express').Router()

// GET all users
router.get('/', authControllers.getAllUsers)

// REGISTER a new user
router.post('/', authControllers.registerUser)

// LOGIN
router.post('/login', authControllers.login)

module.exports = router