const { cartControllers } = require('../controllers/Cart.controllers')

const router = require('express').Router()

router.get('/', cartControllers.getCarts)

router.get('/find/:userId', cartControllers.getCart)

router.post('/', cartControllers.createCart)

router.put('/:id', cartControllers.updateCart)

router.delete('/:id', cartControllers.deleteCart)

module.exports = router